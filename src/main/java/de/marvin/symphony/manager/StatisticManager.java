package de.marvin.symphony.manager;

import com.google.common.collect.Lists;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class StatisticManager {

    @Getter
    private static StatisticManager instance;

    private static final String DATABASE_COLLECTION = "statistics";

    public StatisticManager(String mik, String[] aliases) {
        StatisticManager.instance = this;

        Document document = new Document()
                .append("mik", mik)
                .append("24HoursDocumentCount", this.get24HoursDocumentCount(DATABASE_COLLECTION, "pubDate"))
                .append("documentCount", DatabaseManager.getInstance());
    }

    private int get24HoursDocumentCount(String collection, String key) {
        MongoCollection<Document> mongoCollection = DatabaseManager.getInstance().getMongoDatabase().
                getCollection(DATABASE_COLLECTION);

        FindIterable<Document> findIterable = mongoCollection.find();
        MongoCursor mongoCursor = findIterable.iterator();
        List<Document> documentList = Lists.newArrayList();

        try {
            while (mongoCursor.hasNext())
                documentList.add((Document) mongoCursor.next());
        } finally {
            mongoCursor.close();
        }

        List<Document> countList = Lists.newArrayList();
        documentList.forEach(document -> {
            Date documentDate = document.getDate("pubDate");

            LocalDate localDate = LocalDate.now().minusDays(1);
            Date date = Date.from(localDate.atStartOfDay(ZoneId.of("Europe/Paris")).toInstant());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.ENGLISH);
            formatter.format(date);

            if (documentDate.before(date))
                return;

            countList.add(document);
        });
        return countList.size();
    }
}