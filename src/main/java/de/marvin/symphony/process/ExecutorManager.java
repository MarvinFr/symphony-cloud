package de.marvin.symphony.process;

import com.google.common.collect.Lists;
import de.marvin.symphony.Main;
import lombok.Getter;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Getter
public class ExecutorManager {

    @Getter
    private static ExecutorManager instance;

    private static final String DATABASE_COLLECTION = "modules";

    //list with all executors
    private List<SymphonyExecutor> symphonyExecutorsList;

    private ConcurrentHashMap<Future<?>, SymphonyExecutor> activeExecutorsMap;

    private ExecutorService executorService;

    public ExecutorManager() {
        ExecutorManager.instance = this;

        this.symphonyExecutorsList = Lists.newArrayList();
        this.activeExecutorsMap = new ConcurrentHashMap<>();
        this.executorService = Executors.newCachedThreadPool();
    }

    public void registerExecutor(SymphonyExecutor symphonyExecutor) {
        if (this.symphonyExecutorsList.contains(symphonyExecutor))
            return;

        this.symphonyExecutorsList.add(symphonyExecutor);
    }

    /**
     * start all executors
     * and register as active executor @activeExecutorsListener
     */
    public void startExecutors() {
        this.symphonyExecutorsList.forEach(symphonyExecutor -> {
            //start module
            Future<?> future = this.executorService.submit(symphonyExecutor);
            this.activeExecutorsMap.put(future, symphonyExecutor);
            this.activeExecutorsMap.get(future).setRunning(true);
            System.out.println(Main.PREFIX + symphonyExecutor.getName() + " started");
        });

        new ExecutorListener();
    }

    public void stopExecutors() {
        this.activeExecutorsMap.forEach(((future, symphonyExecutor) -> {
            symphonyExecutor.setRunning(false);
            future.cancel(false);
        }));
    }

    public void startExecutor(String mik) {
        this.symphonyExecutorsList.forEach(symphonyExecutor -> {
            if (!mik.equals(symphonyExecutor.getMik()))
                return;

            if(this.activeExecutorsMap.contains(symphonyExecutor)) {
                System.out.println(Main.PREFIX + symphonyExecutor.getName() + " already started");
                return;
            }
            Future<?> future = this.executorService.submit(symphonyExecutor);
            this.activeExecutorsMap.put(future, symphonyExecutor);
            this.activeExecutorsMap.get(future).setRunning(true);
        });
    }

    /**
     * stop a specific executor by mik
     * @param mik
     */

    public void stopExecutor(String mik) {
        this.activeExecutorsMap.forEach((future, symphonyExecutor) -> {
            if (!mik.equals(symphonyExecutor.getMik()))
                return;
            System.out.println(Main.PREFIX + "stopped " + symphonyExecutor.getName());
            symphonyExecutor.setRunning(false);
            this.activeExecutorsMap.remove(future);
            //TODO: change running status in symphonyExecutorsList???
        });
    }
}
