package de.marvin.symphony.process;

import de.marvin.symphony.Main;
import de.marvin.symphony.setup.SetupManager;
import lombok.Getter;

public class ExecutorListener extends Thread {

    @Getter
    private static ExecutorListener instance;

    private final int EXECUTOR_LISTENER_FREQUENCY;

    public ExecutorListener() {
        ExecutorListener.instance = this;
        //manage modules

        this.EXECUTOR_LISTENER_FREQUENCY = SetupManager.getInstance().getSymphonyConfigFile().get("executorListener").getAsJsonObject().get("repeatFrequency")
                .getAsInt();

        this.start();
    }

    @Override
    public void run() {

        ExecutorManager.getInstance().getActiveExecutorsMap().forEach((future, symphonyExecutor) -> {
            if(future.isDone()) {
                System.out.println(Main.PREFIX + symphonyExecutor.getName() + " is done.");
                symphonyExecutor.run();

            } else {
                System.out.println(Main.PREFIX + symphonyExecutor.getName() + " is running");
            }
        });

        /*
        //TODO: check if cache or db running status has changed
        moduleList.forEach(module -> {
            if(module.getBoolean("running") == null)
                return;
            if (!module.getBoolean("running")) {
                ExecutorManager.getInstance().getSymphonyExecutorsList().forEach(document -> {
                    if(module.getString("mik").equals(document.getMik()))
                        document.setRunning(false);
                });
                return;
            }

            ExecutorManager.getInstance().startExecutor(module.getString("mik"));
            this.runNewsSearch();
        });*/

        //this.runNewsSearch();



        //check if everything is done
        //if (ExecutorManager.getInstance().getActiveExecutorsMap().isEmpty())
        //    return;

        System.out.println(Main.SYMPHONY_EXECUTOR_PREFIX + "ExecutorListener is handling " + ExecutorManager.getInstance().getActiveExecutorsMap().size() + " tasks");

        try {
            sleep(1000 * this.EXECUTOR_LISTENER_FREQUENCY);
        } catch (Exception ex) {
            this.run();
        }
        this.run();
    }

    private void runNewsSearch() {
        ExecutorManager.getInstance().getActiveExecutorsMap().forEach((future, symphonyExecutor) -> {
            if (future.isDone()) {
                System.out.println(Main.SYMPHONY_EXECUTOR_PREFIX + "finished " + symphonyExecutor.getName());
                if (symphonyExecutor.isRunning()) {
                    symphonyExecutor.run();
                } else {
                    ExecutorManager.getInstance().getActiveExecutorsMap().remove(future);
                }
            }
        });
    }
}
