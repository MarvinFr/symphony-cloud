package de.marvin.symphony.process;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class SymphonyExecutor implements Runnable {

    private static final String DATABASE_COLLECTION = "modules";

    private String mik;
    private String name;
    private String description;
    private boolean running;

    public SymphonyExecutor(String mik, String name, String description) {
        this.mik = mik;
        this.name = name;
        this.description = description;
    }

    public abstract void run();

    public void stop() {

    }

}
