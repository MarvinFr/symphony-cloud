package de.marvin.symphony.setup;

import de.marvin.symphony.Main;
import de.marvin.symphony.util.ConsoleColor;
import de.marvin.symphony.util.FileUtils;
import de.marvin.symphony.util.JsonConfiguration;
import lombok.Getter;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

@Getter
public class SetupManager {

    @Getter
    private static SetupManager instance;

    private JsonConfiguration symphonyConfigFile;

    private String mongoConnectionString;

    public SetupManager() {
        SetupManager.instance = this;

        System.out.println("   ____                __                 ");
        System.out.println("  / __/_ ____ _  ___  / /  ___  ___  __ __");
        System.out.println(" _\\ \\/ // /  ' \\/ _ \\/ _ \\/ _ \\/ _ \\/ // /");
        System.out.println("/___/\\_, /_/_/_/ .__/_//_/\\___/_//_/\\_, / ");
        System.out.println("    /___/     /_/                  /___/            SPARK Edition " + Main.VERSION);
        System.out.println();

        System.out.println(Main.PREFIX + "Load config file.. (config.json) ...");
        this.symphonyConfigFile = new JsonConfiguration(FileUtils.createFile(this.getJarFolder(), "config.json"));

        if (!(this.symphonyConfigFile.contains("settings"))) {
            try {
                this.setup();
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        }

        this.mongoConnectionString = this.symphonyConfigFile.get("mongodb").getAsJsonObject().get("connectionString")
                .getAsString();

    }

    private void setup() throws FileNotFoundException, UnsupportedEncodingException {
        JsonConfiguration settings = new JsonConfiguration();
        JsonConfiguration mongodb = new JsonConfiguration();
        JsonConfiguration executorListener = new JsonConfiguration();

        this.clearConsoleScreen();
        System.out.println(this.getJarFolder());
        System.out.println(Main.PREFIX + "Setup mode started" + ConsoleColor.RESET);
        System.out.println(Main.PREFIX + "Create setup file.." + ConsoleColor.RESET);

        mongodb.append("connectionString", "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");

        executorListener.append("repeatFrequency", "30");

        this.symphonyConfigFile.append("settings", settings);
        this.symphonyConfigFile.append("mongodb", mongodb);
        this.symphonyConfigFile.append("executorListener", executorListener);
        this.symphonyConfigFile.save();

        PrintWriter writer = new PrintWriter(this.getJarFolder() + "start.sh", "UTF-8");
        writer.println("#!/bin/bash");
        writer.println("while true");
        writer.println("    do");
        writer.println("        echo \"Server Starting!\"");
        writer.println("        screen -S Symphony java -Xmx1024M -Xms512M -jar " + this.getPathToJar());
        writer.println("        echo \"Server Restarting!\"");
        writer.println("        sleep 5");
        writer.println("done");
        writer.close();

        System.out.println(Main.PREFIX + ConsoleColor.RED_BRIGHT + "Please configure config.json" + ConsoleColor.RESET);
        System.out.println(Main.PREFIX + ConsoleColor.RED_BRIGHT + "Use ./start.sh to start the program");

        System.exit(0);
    }

    public String getJarFolder() {
        String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String[] splitPath = path.split("/");
        String[] newPath = Arrays.copyOf(splitPath, splitPath.length -1);
        System.out.println(Arrays.toString(newPath));
        StringBuilder stringBuilder = new StringBuilder();
        for (String item : newPath)
            stringBuilder.append(item).append("/");
        return stringBuilder.toString();
    }

    public String getPathToJar() {
        return Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    }

    private void clearConsoleScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}
