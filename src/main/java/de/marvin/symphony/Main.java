package de.marvin.symphony;

import de.marvin.symphony.database.DatabaseManager;
import de.marvin.symphony.library.LibraryManager;
import de.marvin.symphony.library.LibrarySettingsManager;
import de.marvin.symphony.newsfeed.NewsFeedManager;
import de.marvin.symphony.newsfeed.NewsModule;
import de.marvin.symphony.newsfeed.SourceManager;
import de.marvin.symphony.process.ExecutorManager;
import de.marvin.symphony.setup.SetupManager;
import de.marvin.symphony.util.ConsoleColor;
import lombok.Getter;

@Getter
public class Main {

    @Getter
    private static Main instance;

    public static final String PREFIX = ConsoleColor.CYAN + "[SYMPHONY-CLOUD] " + ConsoleColor.RESET;
    public static final String SYMPHONY_EXECUTOR_PREFIX = ConsoleColor.RED_BRIGHT + "[SYMPHONY-EXECUTOR] " + ConsoleColor.RESET;
    public static final String VERSION = "3.0 - SNAPSHOT";

    private SetupManager setupManager;

    private DatabaseManager databaseManager;
    private ExecutorManager executorManager;
    private SourceManager sourceManager;
    private NewsFeedManager newsFeedManager;

    //BETA
    private LibrarySettingsManager librarySettingsManager;
    private LibraryManager libraryManager;

    public Main() {
        Main.instance = this;

        this.setupManager = new SetupManager();
        this.databaseManager = new DatabaseManager(SetupManager.getInstance().getMongoConnectionString(), "symphony");

        this.sourceManager = new SourceManager();
        this.sourceManager.loadSources();

        this.newsFeedManager = new NewsFeedManager();

        //BETA
        this.librarySettingsManager = new LibrarySettingsManager();
        this.librarySettingsManager.loadLibrarySources();

        this.libraryManager = new LibraryManager();
        //TODO: add to LibraryModule
        this.libraryManager.loadNotInsertedFiles();

        /*
        CommandController commandController = new CommandController();
        commandController.registerCommand(new SymphonyCommand());
        commandController.registerCommand(new HelpCommand());
        commandController.start();*/


        this.executorManager = new ExecutorManager();
        //this.executorManager.registerExecutor(new AnalysisModule());
        this.executorManager.registerExecutor(new NewsModule());
        //this.executorManager.registerExecutor(new LibraryModule());
        //this.executorManager.registerExecutor(new DataProcessingModule());
        this.executorManager.startExecutors();

    }

    private void shutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutdown Hook is running !");

        }));
        System.out.println("Application Terminating ...");
    }

    public static void main(String[] args) {
        new Main();
    }


}
