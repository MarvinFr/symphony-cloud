package de.marvin.symphony.command;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Command {

    private String name;
    private String description;
    private String usageMessage;
    private String[] aliases;

    public Command(String name, String... aliases) {
        this.name = name;
        this.aliases = aliases;
    }

    public abstract void execute(String[] strings);

}
