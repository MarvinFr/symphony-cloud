package de.marvin.symphony.command;

import de.marvin.symphony.process.ExecutorManager;

public class SymphonyCommand extends Command {

    public SymphonyCommand() {
        super("symphony", "");
    }

    //symphony module list

    @Override
    public void execute(String[] strings) {

        if(strings[0].equalsIgnoreCase("start")) {
            ExecutorManager.getInstance().startExecutor("2");
        }

        if(strings[0].equalsIgnoreCase("stop")) {
            ExecutorManager.getInstance().stopExecutor("2");
        }

    }
}
