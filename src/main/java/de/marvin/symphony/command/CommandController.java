package de.marvin.symphony.command;

import com.google.common.collect.Maps;
import lombok.Getter;

import java.util.*;

public class CommandController extends Thread {


    @Getter
    private static CommandController instance;

    private HashMap<String, Command> commands;

    public CommandController() {
        CommandController.instance = this;

        this.commands = Maps.newHashMap();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print(">> ");
            String line = scanner.nextLine();
            System.out.println("Input Command: " + line);
            String[] splitLine = line.split(" ");

            String commandName = splitLine[0].toLowerCase();
            String[] args = Arrays.copyOfRange(splitLine, 1, splitLine.length);

            Command command = this.getCommand(commandName);

            if (command == null)
                System.out.println("Command does not exits. Please type 'help' for help.");
            else
                command.execute(args);
        }

    }

    private Command getCommand(String name) {
        for (Command command : this.getCommands())
            if (command.getName().equalsIgnoreCase(name))
                return command;
            else
                for (String alias : command.getAliases())
                    if (alias.equalsIgnoreCase(name))
                        return command;

        return null;
    }

    public void registerCommand(Command command) {
        this.commands.put(command.getName().toLowerCase(), command);
    }

    public Collection<Command> getCommands() {
        return new ArrayList<>(this.commands.values());
    }
}
