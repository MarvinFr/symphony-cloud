package de.marvin.symphony.command;

import de.marvin.symphony.Main;

public class HelpCommand extends Command {

    public HelpCommand() {
        super("help", "");
    }

    @Override
    public void execute(String[] strings) {
        System.out.println();
        System.out.println(Main.PREFIX + " ---------- Commands ---------- ");
        System.out.println(Main.PREFIX + "symphony feed add <source> <rssLink> <priority> <language> <category> - Add news feed");
        System.out.println(Main.PREFIX + "symphony feed remove <rssLink> - Remove news feed");
        System.out.println(Main.PREFIX);
        System.out.println(Main.PREFIX + "symphony modules start - Start modules");
        System.out.println(Main.PREFIX + "symphony modules stop - Stop modules");
        System.out.println();
    }
}
