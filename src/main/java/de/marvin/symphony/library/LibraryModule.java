package de.marvin.symphony.library;

import com.google.common.collect.Lists;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import de.marvin.symphony.database.DatabaseManager;
import de.marvin.symphony.process.SymphonyExecutor;
import de.marvin.symphony.setup.SetupManager;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class LibraryModule extends SymphonyExecutor {

    @Getter
    private static LibraryModule instance;

    public LibraryModule() {
        super("2", "LibraryModule", "Collect data from html files");
        LibraryModule.instance = this;
        this.run();
    }

    @SneakyThrows
    @Override
    public void run() {
        List<Document> filesToDownloadList = Lists.newArrayList();
        int filesToDownloadSize = LibraryManager.getInstance().getFilesToDownloadList().size();
        System.out.println("There are " + filesToDownloadList + " files to download");
        if (filesToDownloadSize >= 99)
            filesToDownloadList.addAll(LibraryManager.getInstance().getFilesToDownloadList().subList(0, 99));
        else
            filesToDownloadList.addAll(LibraryManager.getInstance().getFilesToDownloadList());

        System.out.println("I will download " + filesToDownloadList.size());

        filesToDownloadList.forEach(document -> {
            LibraryManager.getInstance().getFilesToDownloadList().remove(document);

            LibrarySettingsManager.getInstance().getLibrarySettingsArrayList().forEach(librarySettings -> {
                if (!(librarySettings.getSIK().equals(document.getString("sik"))))
                    return;

                if (!librarySettings.isDownloadHTML()) {
                    //System.out.println("Not allowed to download: " + document.getString("link"));
                    return;
                }
                System.out.println(librarySettings.getDocument().toString());

                //System.out.println("Files to download: " + LibraryManager.getInstance().getFilesToDownloadList().size());

                try {
                    URL url = new URL(document.getString("link"));
                    final String DIR_KEY = this.createDirKey();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(SetupManager.getInstance().getJarFolder() + "library/" + DIR_KEY + ".html"));

                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        bufferedWriter.write(line);
                    }

                    bufferedReader.close();
                    bufferedWriter.close();
                    //System.out.println("Successfully Downloaded: " + document.getString("link"));

                    org.jsoup.nodes.Document jsoupDocument = Jsoup.parse(new File(SetupManager.getInstance().getJarFolder() + "library/" + DIR_KEY + ".html"), "UTF-8", document.getString("link"));

                    Elements titleElement = jsoupDocument.getElementsByClass(librarySettings.getTITLE());
                    org.jsoup.nodes.Document titleDocument = Jsoup.parse(String.valueOf(titleElement));

                    Elements contentElement = jsoupDocument.getElementsByClass(librarySettings.getCONTENT());
                    org.jsoup.nodes.Document contentDocument = Jsoup.parse(String.valueOf(contentElement));

                    Library library = new Library(document.getString("link"), document.getString("sik"), DIR_KEY, titleDocument.text(), contentDocument.text());
                    //TODO: change from True to false
                    DatabaseManager.getInstance().insertSingleDocument(LibraryManager.DATABASE_COLLECTION, library.getDocument(), true);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        });

        TimeUnit.SECONDS.sleep(60 * ThreadLocalRandom.current().nextInt(2, 50 + 1));
    }

    private String createDirKey() {
        String dirKey = UUID.randomUUID().toString();
        while (this.dirKeyExists(dirKey))
            dirKey = UUID.randomUUID().toString();
        return dirKey;
    }

    private boolean dirKeyExists(String dirKey) {
        return DatabaseManager.getInstance().valueExists(LibraryManager.DATABASE_COLLECTION, "dirKey", dirKey);
    }

    private List<Document> get24HoursDocumentCount(String collection, String key) {
        MongoCollection<Document> mongoCollection = DatabaseManager.getInstance().getMongoDatabase().
                getCollection("newsFeeds");

        FindIterable<Document> findIterable = mongoCollection.find();
        MongoCursor mongoCursor = findIterable.iterator();
        List<Document> documentList = Lists.newArrayList();

        try {
            while (mongoCursor.hasNext())
                documentList.add((Document) mongoCursor.next());
        } finally {
            mongoCursor.close();
        }

        List<Document> countList = Lists.newArrayList();
        documentList.forEach(document -> {
            Date documentDate = document.getDate("pubDate");

            LocalDate localDate = LocalDate.now().minusDays(1);
            Date date = Date.from(localDate.atStartOfDay(ZoneId.of("Europe/Paris")).toInstant());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.ENGLISH);
            formatter.format(date);

            if (documentDate.before(date))
                return;

            countList.add(document);
        });
        return countList;
    }
}
