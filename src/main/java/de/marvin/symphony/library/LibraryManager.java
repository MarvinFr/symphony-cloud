package de.marvin.symphony.library;

import com.google.common.collect.Lists;
import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;
import org.bson.Document;

import java.util.List;

@Getter
public class LibraryManager {

    @Getter
    private static LibraryManager instance;

    public static final String DATABASE_COLLECTION = "newsLibrary";

    private List<Document> filesToDownloadList;

    public LibraryManager() {
        LibraryManager.instance = this;

        this.filesToDownloadList = Lists.newArrayList();

    }

    //TODO: gives 0 back :(
    public List<Document> loadNotInsertedFiles() {
        List<Document> librarySettingsList = DatabaseManager.getInstance().getDocumentsFromCollection(LibrarySettingsManager.DATABASE_COLLECTION);

        List<Document> newsFeedsList = Lists.newArrayList();

        librarySettingsList.forEach(document -> {
            newsFeedsList.addAll(DatabaseManager.getInstance().getDocumentsByKeyValue("newsFeeds", "sik", document.getString("sik")));
        });

        newsFeedsList.forEach(document -> {
            if(DatabaseManager.getInstance().getDocumentByKeyValue(DATABASE_COLLECTION, "link", document.getString("link")) == null)
                this.filesToDownloadList.add(document);
        });
        System.out.println("Not inserted feeds: " + this.filesToDownloadList.size());
        return this.filesToDownloadList;
    }

    /***
     * Method to check if html file is already registered
     * @param link from newsFeed collection
     * @return boolean
     */

    private boolean alreadyInserted(String link) {
        return DatabaseManager.getInstance().getDocumentByKeyValue(DATABASE_COLLECTION, "sik", link) != null;
    }


}
