package de.marvin.symphony.library;

import lombok.Getter;
import org.bson.Document;

@Getter
public class LibrarySettings {

    private final String SIK;
    private final String SOURCE;
    private final String TITLE;
    private final String CONTENT;
    private boolean downloadHTML;

    public LibrarySettings(String sik, String source, String title, String content, boolean downloadHTML) {
        this.SIK = sik;
        this.SOURCE = source;
        this.TITLE = title;
        this.CONTENT = content;
        this.downloadHTML = downloadHTML;
    }

    public LibrarySettings(String sik, String source, String title, String content) {
        this.SIK = sik;
        this.SOURCE = source;
        this.TITLE = title;
        this.CONTENT = content;
        this.downloadHTML = false;
    }

    public Document getDocument() {
        return new Document()
                .append("sik", this.SIK)
                .append("source", this.SOURCE)
                .append("title", this.TITLE)
                .append("content", this.CONTENT)
                .append("downloadHTML", this.downloadHTML);
    }
}
