package de.marvin.symphony.library;


import com.google.common.collect.Lists;
import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;

import java.util.List;

/***
 * controlls html text search
 */

@Getter
public class LibrarySettingsManager {

    @Getter
    private static LibrarySettingsManager instance;

    public static final String DATABASE_COLLECTION = "librarySettings";

    private List<LibrarySettings> librarySettingsArrayList;

    public LibrarySettingsManager() {
        LibrarySettingsManager.instance = this;

        this.librarySettingsArrayList = Lists.newArrayList();
    }

    public void addLibrarySource(LibrarySettings librarySettings) {
        if(this.librarySettingsArrayList.contains(librarySettings))
            return;
        this.librarySettingsArrayList.add(librarySettings);
        if(DatabaseManager.getInstance().getDocumentByKeyValue(DATABASE_COLLECTION, "sik", librarySettings.getSIK()) == null)
            DatabaseManager.getInstance().insertSingleDocument(DATABASE_COLLECTION, librarySettings.getDocument(), false);
    }

    public void removeLibrarySource(String rssLink) {
        DatabaseManager.getInstance().deleteDocumentByKeyValue(DATABASE_COLLECTION, "rssLink", rssLink);
    }

    public void loadLibrarySources() {
        DatabaseManager.getInstance().getDocumentsFromCollection(DATABASE_COLLECTION).forEach(document -> {

            if(document.getBoolean("downloadHTML"))
                this.addLibrarySource(new LibrarySettings(
                        document.getString("sik"),
                        document.getString("source"),
                        document.getString("title"),
                        document.getString("content"),
                        document.getBoolean("downloadHTML")));
        });
    }




}
