package de.marvin.symphony.library;

import lombok.Getter;
import org.bson.Document;

@Getter
public class Library {

    private final String LINK;
    private final String SIK;
    private final String DIR_KEY;
    private final String TITLE;
    private final String CONTENT;

    public Library(String link, String sik, String dirKey, String title, String content) {
        this.LINK = link;
        this.SIK = sik;
        this.DIR_KEY = dirKey;
        this.TITLE = title;
        this.CONTENT = content;
    }

    public Document getDocument() {
        return new Document()
                .append("link", this.LINK)
                .append("sik", this.SIK)
                .append("dirKey", this.DIR_KEY)
                .append("title", this.TITLE)
                .append("content", this.CONTENT);
    }
}
