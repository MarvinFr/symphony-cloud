package de.marvin.symphony.newsfeed;

import com.google.common.collect.Lists;
import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;
import org.bson.Document;

import java.util.List;

@Getter
public class SourceManager {

    @Getter
    private static SourceManager instance;

    private static final String DATABASE_COLLECTION = "newsSources";

    private List<Source> sourceCache;

    public SourceManager() {
        SourceManager.instance = this;

        this.sourceCache = Lists.newArrayList();
    }

    /**
     * load sources from database
     */

    public void loadSources() {
        List<Document> documents = DatabaseManager.getInstance().getDocumentsFromCollection(DATABASE_COLLECTION);

        documents.forEach(document -> this.sourceCache.add(new Source(document.getString("sik"),
                document.getString("source"),
                document.getString("rssLink"),
                document.getString("category"),
                document.getString("priority"),
                document.getString("language"),
                document.getBoolean("downloadHTML"))));
    }

    /**
     * add source object into database
     * @param source
     */

    public void addSource(Source source) {
        if (DatabaseManager.getInstance().valueExists(DATABASE_COLLECTION, "rssLink", source.getRssLink()))
            return;
        DatabaseManager.getInstance().insertSingleDocument(DATABASE_COLLECTION, this.getDocument(source), false);
}

    /**
     * remove a specific source from cache and database
     * @param rssLink
     */

    public void removeSource(String rssLink) {
        this.sourceCache.forEach(source -> {
            if (rssLink.equals(source.getRssLink()))
                this.sourceCache.remove(source);
        });
        DatabaseManager.getInstance().deleteDocumentByKeyValue(DATABASE_COLLECTION, "rssLink", rssLink);
    }

    /**
     * return document
     * @param source
     * @return
     */

    public Document getDocument(Source source) {
        return new Document()
                .append("sik", source.getSik())
                .append("source", source.getSource())
                .append("rssLink", source.getRssLink())
                .append("category", source.getCategory())
                .append("priority", source.getPriority())
                .append("language", source.getLanguage())
                .append("downloadHTML", source.isDownloadHTML());
    }
}
