package de.marvin.symphony.newsfeed;

import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;
import org.bson.Document;

public class NewsFeedManager {

    @Getter
    private static NewsFeedManager instance;

    private static final String DATABASE_COLLECTION = "newsFeeds";

    public NewsFeedManager() {
        NewsFeedManager.instance = this;
    }

    public void insertNewsFeed(NewsFeed newsFeed, String sik) {
        if (DatabaseManager.getInstance().valueExists(DATABASE_COLLECTION, "link", newsFeed.getLink()))
            return;
        DatabaseManager.getInstance().insertSingleDocument(DATABASE_COLLECTION, this.getDocument(newsFeed, sik), true);
    }

    private Document getDocument(NewsFeed newsFeed, String sik) {
        return new Document()
                .append("link", newsFeed.getLink())
                .append("sik", sik)
                .append("title", newsFeed.getTitle())
                .append("description", newsFeed.getDescription())
                .append("image", newsFeed.getImageUrl())
                .append("pubDate", newsFeed.getPubDate());
    }
}
