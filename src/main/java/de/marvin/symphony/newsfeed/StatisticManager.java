package de.marvin.symphony.newsfeed;

import com.google.common.collect.Lists;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//TODO: Make it better
public class StatisticManager {

    @Getter
    private static StatisticManager instance;

    private static final String DATABASE_COLLECTION = "modules";

    public StatisticManager() {
        StatisticManager.instance = this;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.ENGLISH);
        Date date = new Date();
        formatter.format(date);

        Document document = new Document()
                .append("cloudStatus", "NONE")
                .append("lastStart", date)
                .append("24HourNewsFeedCount", this.get24HoursNewsFeedCount());

    }

    private int get24HoursNewsFeedCount() {
        MongoCollection<Document> mongoCollection = DatabaseManager.getInstance().getMongoDatabase().getCollection(DATABASE_COLLECTION);

        FindIterable<Document> findIterable = mongoCollection.find();
        MongoCursor mongoCursor = findIterable.iterator();
        List<Document> documentList = Lists.newArrayList();
        try {
            while (mongoCursor.hasNext())
                documentList.add((Document) mongoCursor.next());
        } finally {
            mongoCursor.close();
        }
        List<Document> countList = Lists.newArrayList();
        documentList.forEach(document -> {
            Date documentDate = document.getDate("pubDate");

            LocalDate localDate = LocalDate.now().minusDays(1);
            Date date = Date.from(localDate.atStartOfDay(ZoneId.of("Europe/Paris")).toInstant());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.ENGLISH);
            formatter.format(date);

            if(documentDate.before(date))
                return;

            countList.add(document);
        });
        return countList.size();
    }

}
