package de.marvin.symphony.newsfeed;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class NewsFeed {

    @Getter
    private static NewsFeed instance;

    private String link;
    private String title;
    private String description;
    private String imageUrl;
    private Date pubDate;

    public NewsFeed() {
        NewsFeed.instance = this;
    }
}
