package de.marvin.symphony.newsfeed;

import de.marvin.symphony.process.SymphonyExecutor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

public class NewsModule extends SymphonyExecutor {

    private URL url;

    private String link;
    private String title;
    private String description;
    private String enclosure;

    private String sik;

    public NewsModule() {
        super("2", "NewsModule", "find and search news");

    }

    @Override
    public void run() {
        System.out.println("Source cache size: " + SourceManager.getInstance().getSourceCache().size());
        //TODO: Fix error InputStream cannot be null,
        SourceManager.getInstance().getSourceCache().forEach(source -> {
            try {
                this.url = new URL(source.getRssLink());
                this.sik = source.getSik();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (url == null)
                return;
            this.read();
        });
    }

    public void read() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            if (this.getUrlStream() == null)
                return;

            if (documentBuilder.parse(this.getUrlStream()) == null)
                return;

            Document document = documentBuilder.parse(this.getUrlStream());

            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("item");

            for (int i = 0; i <= nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                Element element = (Element) node;

                this.link = element.getElementsByTagName("link").item(0).getTextContent();
                this.title = element.getElementsByTagName("title").item(0).getTextContent();
                this.description = element.getElementsByTagName("description").item(0).getTextContent();
                String pubDateFromFeed = element.getElementsByTagName("pubDate").item(0).getTextContent();


                this.link = this.link.replace("<![CDATA[", "");
                this.link = this.link.replace("]]>", "");

                this.title = this.title.replace("<![CDATA[", "");
                this.title = this.title.replace("]]>", "");

                this.description = this.description.replace("<![CDATA[", "");
                this.description = this.description.replace("]]>", "");

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.ENGLISH);
                //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.ENGLISH);
                Date date = null;
                try {
                    date = simpleDateFormat.parse(pubDateFromFeed);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                //TODO: Time
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.ENGLISH);

                formatter.format(date);
                Date pubDate = date;
                XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

                InputStream inputStream = this.getUrlStream();

                try {
                    XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(inputStream);


                    while (xmlEventReader.hasNext()) {

                        XMLEvent xmlEvent = xmlEventReader.nextEvent();
                        if (xmlEvent.isStartElement()) {

                            String localPart = xmlEvent.asStartElement().getName().getLocalPart();

                            this.checkForImage(xmlEvent);
                            //Sysout unknown values with var localPart

                        } else if (xmlEvent.isEndElement()) {
                            if (xmlEvent.asEndElement().getName().getLocalPart().equalsIgnoreCase("item")) {

                                NewsFeed newsFeed = new NewsFeed();
                                newsFeed.setLink(this.link);
                                newsFeed.setTitle(this.title);
                                newsFeed.setDescription(this.description);
                                newsFeed.setPubDate(pubDate);
                                newsFeed.setImageUrl(this.enclosure);
                                NewsFeedManager.getInstance().insertNewsFeed(newsFeed, sik);

                                xmlEventReader.nextEvent();
                            }
                        }
                    }
                } catch (XMLStreamException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException | NullPointerException ignored) {

        }
    }

    private InputStream getUrlStream() {
        try {
            return this.url.openStream();
        } catch (IOException ex) {
            return null;
        }
    }

    private void checkForImage(XMLEvent xmlEvent) {
        Iterator<Attribute> attributes = xmlEvent.asStartElement().getAttributes();
        try {
            while (attributes.hasNext()) {
                Attribute currentAttribute = attributes.next();
                if (currentAttribute.getName().toString().equals("url")) {
                    this.enclosure = currentAttribute.getValue();
                } else {
                    this.enclosure = "";
                }
            }
        } catch (Exception ex) {

        }
    }


}
