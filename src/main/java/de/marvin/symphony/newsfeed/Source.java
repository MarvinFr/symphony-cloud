package de.marvin.symphony.newsfeed;

import de.marvin.symphony.database.DatabaseManager;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class Source {

    @Getter
    private static Source instance;

    private final String collection = "newsSources";

    private String sik;
    private String source;
    private String rssLink;
    private String category;
    private String priority;
    private String language;
    private boolean downloadHTML;

    public Source(String sik, String source, String rssLink, String category, String priority, String language, boolean downloadHTML) {
        Source.instance = this;

        this.sik = sik;
        this.source = source;
        this.rssLink = rssLink;
        this.category = category.toUpperCase();
        this.setPriority(priority);
        this.language = language.toUpperCase();
        this.downloadHTML = downloadHTML;
    }

    public Source(String sik, String source, String rssLink, String category, String priority, String language) {
        Source.instance = this;

        this.sik = sik;
        this.source = source;
        this.rssLink = rssLink;
        this.category = category.toUpperCase();
        this.setPriority(priority);
        this.language = language.toUpperCase();
        this.downloadHTML = false;

    }

    public Source(String source, String rssLink, String category, String priority, String language) {
        Source.instance = this;

        this.sik = this.createSik();
        this.source = source;
        this.rssLink = rssLink;
        this.category = category.toUpperCase();
        this.setPriority(priority);
        this.language = language.toUpperCase();
        this.downloadHTML = false;

    }

    private void setPriority(String priority) {
        if (priority.equalsIgnoreCase("high"))
            this.priority = "HIGH";
        else if (priority.equalsIgnoreCase("middle"))
            this.priority = "MIDDLE";
        else
            this.priority = "LOW";
    }

    private String createSik() {
        this.sik = UUID.randomUUID().toString();
        while (this.sikExists(this.sik))
            this.sik = UUID.randomUUID().toString();
        return this.sik;
    }

    private boolean sikExists(String sik) {
        return DatabaseManager.getInstance().valueExists(this.collection, "sik", sik);
    }
}
