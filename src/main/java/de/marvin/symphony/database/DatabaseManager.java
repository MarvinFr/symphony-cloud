package de.marvin.symphony.database;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import lombok.Getter;
import org.bson.Document;

import java.util.List;

@Getter
public class DatabaseManager {

    @Getter
    private static DatabaseManager instance;

    private MongoDatabase mongoDatabase;

    public DatabaseManager(String connectionString, String database) {
        DatabaseManager.instance = this;

        MongoClient mongoClient = new MongoClient(new MongoClientURI(connectionString));
        this.mongoDatabase = mongoClient.getDatabase(database);
    }

    /***
     * Insert a single document
     * @param collection your database collection
     * @param document the document you like to insert
     * @param insertIfDocumentExists value to handle if void should check whether document exist or not
     */

    public void insertSingleDocument(String collection, Document document, boolean insertIfDocumentExists) {
        if (documentExists(collection, document) && !insertIfDocumentExists)
            return;
        //Insert document
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        mongoCollection.insertOne(document);
    }

    /***
     * Insert multiple documents
     * @param collection your database collection
     * @param documentList a list of documents you like to insert
     * @param insertIfDocumentExists value to handle if void should check whether document exist or not
     */

    public void insertManyDocuments(String collection, List<Document> documentList, boolean insertIfDocumentExists) {
        List<Document> newDocumentList = Lists.newArrayList();
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);

        //do not insert if document already exists
        if(!insertIfDocumentExists) {
            documentList.forEach(document -> {
                if (!documentExists(collection, document))
                    newDocumentList.add(document);
            });
            if (!newDocumentList.isEmpty())
                mongoCollection.insertMany(newDocumentList);
            return;
        }

        mongoCollection.insertMany(documentList);

    }

    /***
     * Delete a single document
     * @param collection your database collection
     * @param document the document you like to insert
     */

    public void deleteOneDocument(String collection, Document document) {
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        mongoCollection.deleteOne(document);
    }

    public void deleteManyDocuments(String collection, Document document) {
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        mongoCollection.deleteMany(document);
    }

    public void deleteDocumentByKeyValue(String collection, String key, String value) {
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        Document document = mongoCollection.find(Filters.eq(key, value)).first();
        mongoCollection.deleteOne(document);
    }

    public void updateDocumentByKeyValue(String collection, String searchKey, String searchValue, String newKey, String newValue) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put(searchKey, searchValue);

        BasicDBObject replaceQuery = new BasicDBObject();
        replaceQuery.put(newKey, newValue);

        BasicDBObject updateObject = new BasicDBObject();
        updateObject.put("$set", replaceQuery);

        this.mongoDatabase.getCollection(collection).updateOne(searchQuery, updateObject);
    }

    /***
     * Check if the given document exists in collection
     * @param collection your database collection
     * @param document the document you like to insert
     * @return boolean to check whether document exists or not
     */

    public boolean documentExists(String collection, Document document) {
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        document = mongoCollection.find(document).first();
        return document != null;
    }

    /***
     * Check if a String value exists
     * @param collection your database collection
     * @param key
     * @param value
     * @return
     */

    public boolean valueExists(String collection, String key, String value) {
        Document feedDocument = this.mongoDatabase.getCollection(collection).find(Filters.eq(key, value)).first();
        return feedDocument != null;
    }

    public Document getDocumentByKeyValue(String collection, String key, String value) {
        return this.mongoDatabase.getCollection(collection).find(Filters.eq(key, value)).first();
    }

    public List<Document> getDocumentsByKeyValue(String collection, String key, String value) {
        List<Document> documentList = Lists.newArrayList();
        this.mongoDatabase.getCollection(collection).find(Filters.eq(key, value)).forEach((Block<? super Document>) documentList::add);
        return documentList;
    }

    /***
     * Return a list of all documents in a collection
     * @param collection your database collection
     * @return a list of documents
     */

    public List<Document> getDocumentsFromCollection(String collection) {
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        FindIterable<Document> findIterable = mongoCollection.find();
        MongoCursor mongoCursor = findIterable.iterator();
        List<Document> documentList = Lists.newArrayList();
        try {
            while (mongoCursor.hasNext())
                documentList.add((Document) mongoCursor.next());
        } finally {
            mongoCursor.close();
        }
        return documentList;
    }

    public void updateDocument(String collection, String searchKey, String searchValue, Document newDocument) {
        MongoCollection<Document> mongoCollection = this.mongoDatabase.getCollection(collection);
        mongoCollection.updateOne(Filters.eq(searchKey, searchValue), newDocument);
    }
}
